const Article = (_, res) => {
  var d = new Date();
  var n = d.getTime();
  res.statusCode = 200
  res.json({
    id: 1,
    author: 'Andrea Vassallo A',
    company: 'Nebulab',
    image_url: `http://placekitten.com/200/200?ts=${n}`,
    content: 'The cat is a domestic species of small carnivorous mammal.'
  })
}

export default Article