const { join } = require('path');

module.exports = {
  // content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  content: [join(__dirname, './pages/**/*.{js,ts,jsx,tsx}')],
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: []
}